cmake_minimum_required(VERSION 2.8)
project(UsersGuides NONE)
include(CMakeDependentOption)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_SOURCE_DIR}/CMake)

option(ENABLE_PARAVIEW_GUIDE "Build 'The ParaView Guide'" ON)
cmake_dependent_option(ENABLE_PARAVIEW_GUIDE_EXTRAS
  "Build 'The ParaView Guide with Extras' (non-CE)" OFF
  ENABLE_PARAVIEW_GUIDE OFF)
mark_as_advanced(ENABLE_PARAVIEW_GUIDE_EXTRAS)

if(ENABLE_PARAVIEW_GUIDE)
  add_subdirectory(ParaView)
endif()

option(ENABLE_CATALYST_USERS_GUIDE "Build 'Catalyst User's Guide'" OFF)
if(ENABLE_CATALYST_USERS_GUIDE)
  add_subdirectory(ParaViewCatalyst)
endif()

if(WIN32)
  find_program(PDFLATEX_EXECUTABLE
    NAMES pdflatex
    PATH_SUFFIXES
      "MiKTeX 2.9/miktex/bin"
    )
  if(NOT PDFLATEX_EXECUTABLE)
    message(FATAL_ERROR "pdflatex.exe not found.  "
      "Set PDFLATEX_EXECUTABLE to the location of a pdflatex "
      "with at least these packages installed:
 framed
 url
 ")
  endif()
  get_filename_component(pdflatex_dir "${PDFLATEX_EXECUTABLE}" PATH)
  string(REPLACE "/" "\\" pdflatex_dir "${pdflatex_dir}")
  configure_file(Utilities/Scripts/make-latex.cmd.in make-latex.cmd @ONLY)
endif()
